module.exports = {
  configureWebpack: {
    devServer: {
      overlay: { warnings: true, errors: true },
    },
  },
  lintOnSave: true,
}