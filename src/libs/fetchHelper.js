/** Object wrapper for common proccess some responce. For example Auth state */

const fetchHelper = function (URL, OPT) {
  return fetch(URL, OPT)
    .then((response) => {
      /** If not okey response */
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response.json();
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

export default fetchHelper;