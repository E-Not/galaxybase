import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/1/'
    },
    {
      path: '/:page/',
      name: 'list',
      component: () => import(/* webpackChunkName: "list" */ '@/pages/List.vue'),
    },
    {
      path: '/ship/:id',
      name: 'item',
      component: () => import(/* webpackChunkName: "item" */ '@/pages/Item.vue')
    },
    {
      path: '*',
      name: 'e404',
      component: () => import(/* webpackChunkName: "e404" */ '@/pages/E404.vue')
    }
  ]
})
