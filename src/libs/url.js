const URL = {
  build: (path = '', params = []) => {
    if (params.length) {
      return path + params.join('/');
    }
    else {
      return path;
    }
  },
  __default: {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
  },
  base: 'https://swapi.co/api/starships/',
};

export default URL;